# Ruskin's Docker Compose Files

## Run
1. Run the file using `docker-compose -f FILE.yml up -d`
1. Stop the containers using `docker-compose -f FILE.yml down`
1. Stop the containers and remove the volumes using `docker-compose -f FILE.yml down -v`

## Interacting with the volumes
1. Copy files into the volume using `docker run --rm -v $PWD:/source -v VOLUME_NAME:/dest -w /source alpine cp FILE_NAME /dest`
    1. VOLUME_NAME  
        * is the name of the volume you have created
        * volumes can be inspected using `docker volume ls` and `docker volume inspect VOLUME_NAME`
    1. FILE_NAME
        * is the name of the file in your PWD (present working directory) you would like to copy into the volume
        
## Diving into xhyve on Mac
1. Run the screen command `screen ~/Library/Containers/com.docker.docker/Data/com.docker.driver.amd64-linux/tty`
1. List all the volumes available in the XHYVE VM using `ls -ltrh /var/lib/docker/volumes`